package ru.nickmiller.tfhomework10

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.EditText
import android.widget.Toast
import ru.nickmiller.tfhomework10.data.AppDatabase
import ru.nickmiller.tfhomework10.data.NodeEntity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val nodeValue = findViewById<EditText>(R.id.node_value)
        val recycler = findViewById<RecyclerView>(R.id.nodes_recycler)
        recycler.layoutManager = GridLayoutManager(this, 5)

        val db = AppDatabase.getAppDataBase(this)
        val dao = db.nodeDao()

        val adapter = NodesAdapter()
        adapter.onNodeClickListener = {
            startActivity(Intent(this, NodeDetailsActivity::class.java).apply {
                putExtra("node_id", it.node.id)
            })
        }

        recycler.adapter = adapter

        dao.getNodesWithChildren().observe(this, Observer {
            adapter.nodes = it!!.toMutableList()
            adapter.notifyDataSetChanged()
        })

        findViewById<View>(R.id.fab).setOnClickListener {
            val value = nodeValue.text.toString()

            if (value.isNotEmpty()) {
                Thread(Runnable {
                    dao.addNode(NodeEntity(value = value.toInt()))
                    nodeValue.text.clear()
                }).start()
            } else {
                Toast.makeText(this, "Введите значение", Toast.LENGTH_SHORT).show()
            }

        }
    }
}
