package ru.nickmiller.tfhomework10.data

import android.arch.persistence.room.*
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
@Entity(tableName = "nodes")
data class NodeEntity(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    var value: Int = 0
) : Parcelable


// Модель, хранящая отношение двух Node
@Parcelize
@Entity(
    tableName = "node_child",
    primaryKeys = ["node_id", "child_id"])
data class NodeAndChild(

    @ColumnInfo(name = "node_id")
    @ForeignKey(
        entity = NodeEntity::class,
        parentColumns = ["id"],
        childColumns = ["node_id"],
        onDelete = ForeignKey.CASCADE)
    var nodeId: Long = 0,


    @ColumnInfo(name = "child_id")
    @ForeignKey(
        entity = NodeEntity::class,
        parentColumns = ["id"],
        childColumns = ["child_id"],
        onDelete = ForeignKey.CASCADE)
    var childId: Long = 0

) : Parcelable


@Parcelize
data class NodeWithChildren(
    @Embedded var node: NodeEntity = NodeEntity(),

    @Relation(parentColumn = "id", entityColumn = "node_id")
    var children: MutableList<NodeAndChild> = mutableListOf()
) : Parcelable



// Возвращает родителей Node
fun NodeWithChildren.findParentsIn(nodes: List<NodeWithChildren>): MutableList<NodeWithChildren> {
    val parents = mutableListOf<NodeWithChildren>()

    nodes.flatMap { it.children }.forEach { nac->
            if (nac.childId == node.id) {
                val parent = nodes.find { it.node.id == nac.nodeId }
                parent?.let { parents.add(it) }
            }
        }

    return parents
}


// Возврвщвет детей Node
fun NodeWithChildren.findChildrenIn(nodes: List<NodeWithChildren>): MutableList<NodeWithChildren> {
    val res = mutableListOf<NodeWithChildren>()

    children.forEach { children ->
        val ch = nodes.find { it.node.id == children.childId }
        ch?.let { res.add(it) }
    }
    return res
}


// Возвращает true, если Node, на которой вызвана функция,
// является родителем Node, переданной в качестве параметра
fun NodeWithChildren.isParentOf(node: NodeWithChildren): Boolean {
    children.forEach {
        if (it.childId == node.node.id) {
            return true
        }
    }
    return false
}