package ru.nickmiller.tfhomework10.data

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*


@Dao
interface NodeDao {

    @Query("SELECT * from nodes")
    fun getNodesWithChildren(): LiveData<List<NodeWithChildren>>


    @Insert
    fun addNode(node: NodeEntity)

    @Query("SELECT * FROM nodes")
    fun getAllNodes(): LiveData<List<NodeEntity>>

    @Query("SELECT * FROM node_child")
    fun getRelations(): LiveData<List<NodeAndChild>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addRelation(relation: NodeAndChild)

    @Delete
    fun removeRelation(relation: NodeAndChild)
}