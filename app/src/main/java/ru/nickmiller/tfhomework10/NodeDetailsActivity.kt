package ru.nickmiller.tfhomework10

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_node_details.*
import ru.nickmiller.tfhomework10.NodesFragment.Companion.CHILDREN_TYPE
import ru.nickmiller.tfhomework10.NodesFragment.Companion.PARENTS_TYPE

class NodeDetailsActivity : AppCompatActivity() {

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private var currentNodeId: Long = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_node_details)

        currentNodeId = intent.getLongExtra("node_id", -1)
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        container.adapter = mSectionsPagerAdapter
        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))
    }


    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return if (position == 0) {
                NodesFragment.newInstance(CHILDREN_TYPE, currentNodeId)
            } else {
                NodesFragment.newInstance(PARENTS_TYPE, currentNodeId)
            }

        }

        override fun getCount() = 2
    }
}
