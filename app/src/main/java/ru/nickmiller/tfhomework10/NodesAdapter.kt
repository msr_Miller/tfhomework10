package ru.nickmiller.tfhomework10

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.nickmiller.tfhomework10.data.NodeWithChildren
import ru.nickmiller.tfhomework10.data.findParentsIn
import ru.nickmiller.tfhomework10.data.isParentOf


class NodesAdapter(
    var currentNode: NodeWithChildren? = null,
    var nodes: MutableList<NodeWithChildren> = mutableListOf(),
    val details: Boolean = false
) :
    RecyclerView.Adapter<NodesAdapter.ViewHolder>() {

    var onNodeClickListener: ((nodeWithChildren: NodeWithChildren) -> Unit)? = null

    val DEFAULT_VIEW_TYPE = 1
    val WITH_CHILDREN_VIEW_TYPE = 2
    val WITH_PARENTS_VIEW_TYPE = 3
    val WITH_CHILDREN_AND_PARENTS_VIEW_TYPE = 4
    val DETAILS_WITH_RELATIONS = 5
    val DETAILS_DEFAULT = 6

    override fun onCreateViewHolder(paarent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(paarent.context).inflate(R.layout.view_node, paarent, false)
        val bg = when (viewType) {
            WITH_CHILDREN_VIEW_TYPE -> Color.YELLOW
            WITH_PARENTS_VIEW_TYPE -> Color.BLUE
            WITH_CHILDREN_AND_PARENTS_VIEW_TYPE -> Color.RED
            DETAILS_WITH_RELATIONS -> Color.GREEN
            else -> Color.TRANSPARENT
        }
        view.setBackgroundColor(bg)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.nodeValue.text = nodes[position].node.value.toString()
        holder.itemView.setOnClickListener {
            onNodeClickListener?.invoke(nodes[position])
        }

    }

    override fun getItemCount() = nodes.size

    override fun getItemViewType(position: Int): Int {
        val n = nodes[position]
        val parents = nodes[position].findParentsIn(nodes)

        if (!details) {
            return if (n.children.isNotEmpty() && parents.isNotEmpty()) {
                WITH_CHILDREN_AND_PARENTS_VIEW_TYPE
            } else if (n.children.isNotEmpty()) {
                WITH_CHILDREN_VIEW_TYPE
            } else if (parents.isNotEmpty()) {
                WITH_PARENTS_VIEW_TYPE
            } else {
                DEFAULT_VIEW_TYPE
            }
        } else {
            return if (currentNode?.let { n.isParentOf(it) || it.isParentOf(n) } == true) {
                DETAILS_WITH_RELATIONS
            } else {
                DETAILS_DEFAULT
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nodeValue = itemView.findViewById<TextView>(R.id.node_value)
    }
}