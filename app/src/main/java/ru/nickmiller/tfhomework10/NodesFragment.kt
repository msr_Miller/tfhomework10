package ru.nickmiller.tfhomework10

import android.arch.lifecycle.Observer
import android.arch.lifecycle.Transformations
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_node_details.view.*
import ru.nickmiller.tfhomework10.data.*


class NodesFragment : Fragment() {
    lateinit var dao: NodeDao

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_node_details, container, false)
        val recycler = rootView.nodes_recycler
        recycler.layoutManager = GridLayoutManager(context, 5)
        val currentNodeId = arguments?.getLong(NODE_ID)
        val type = arguments?.getInt(TYPE)

        val db = AppDatabase.getAppDataBase(context!!)
        dao = db.nodeDao()

        val adapter = NodesAdapter(details = true)
        var currentNode: NodeWithChildren? = null


        // LiveData реагирующая на изменение отношений в базе данных
        Transformations.switchMap(dao.getRelations()) {
            dao.getNodesWithChildren()
        }.observe(this, Observer {
            currentNode = it?.find { it.node.id == currentNodeId }!!

            val parents = currentNode?.findParentsIn(it) ?: mutableListOf()
            val children = currentNode?.findChildrenIn(it) ?: mutableListOf()

            val sNodes = mutableListOf<NodeWithChildren>()
            if (type == PARENTS_TYPE) {
                sNodes.addAll(parents)
            } else {
                sNodes.addAll(children)
            }

            it.forEach { nwc ->
                if (!nwc.isParentOf(currentNode!!) && !currentNode!!.isParentOf(nwc) && nwc != currentNode) {
                    sNodes.add(nwc)
                }
            }
            adapter.currentNode = currentNode
            adapter.nodes = sNodes
            adapter.notifyDataSetChanged()
        })

        // При клике на Node, добавляем/удаляем отношение
        adapter.onNodeClickListener = {
            if (type == PARENTS_TYPE) {
                if (it.isParentOf(currentNode!!)) {
                    removeRelation(it.node.id, currentNode!!.node.id)
                } else {
                    addRelation(it.node.id, currentNode!!.node.id)
                }
            } else {
                if (currentNode!!.isParentOf(it)) {
                    removeRelation(currentNode!!.node.id, it.node.id)
                } else {
                    addRelation(currentNode!!.node.id, it.node.id)
                }
            }
        }
        recycler.adapter = adapter
        return rootView
    }


    private fun addRelation(parent: Long, child: Long) {
        Thread {
            dao.addRelation(
                NodeAndChild(
                    nodeId = parent,
                    childId = child
                )
            )
        }.start()
    }

    private fun removeRelation(parent: Long, child: Long) {
        Thread {
            dao.removeRelation(
                NodeAndChild(
                    nodeId = parent,
                    childId = child
                )
            )
        }.start()
    }

    companion object {
        val PARENTS_TYPE = 0
        val CHILDREN_TYPE = 1
        private val TYPE = "type"
        private val NODE_ID = "node_id"

        fun newInstance(type: Int, nodeId: Long): NodesFragment {
            val fragment = NodesFragment()
            val args = Bundle()
            args.putInt(TYPE, type)
            args.putLong(NODE_ID, nodeId)
            fragment.arguments = args
            return fragment
        }
    }
}